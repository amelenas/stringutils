package com.stepanovich.gradleproject;

import static org.apache.commons.lang3.StringUtils.isNumeric;

public class StringUtils {
    private StringUtils() {
    }

    public static boolean isPositiveNumber(String str) {
        if (isNumeric(str)) {
            return Integer.parseInt(str) >= 0;
        }
        return false;
    }
}
