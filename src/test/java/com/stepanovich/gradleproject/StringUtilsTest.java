package com.stepanovich.gradleproject;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {

    @Test
    void StringUtilsNotNumber() {
        String str = "text";
        boolean result = StringUtils.isPositiveNumber(str);
        assertFalse(result);
    }

    @Test
    void StringUtilsNPositive() {
        String str = "10";
        boolean result = StringUtils.isPositiveNumber(str);
        assertTrue(result);
    }

    @Test
    void StringUtilsNegative() {
        String str = "-10";
        boolean result = StringUtils.isPositiveNumber(str);
        assertFalse(result);
    }

    @Test
    void StringUtilsNull() {
        boolean result = StringUtils.isPositiveNumber(null);
        assertFalse(result);
    }
}